import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { DatabaseMigrationService } from 'db/migrationService';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
    
  const databaseMigrationService = app.get(DatabaseMigrationService);
  await databaseMigrationService.migrateDatabase();

  await app.listen(3000);
}
bootstrap();
