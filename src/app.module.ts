import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { DatabaseService } from '../db/db';
import { DatabaseMigrationService } from '../db/migrationService';
import { SocketGateway } from './socket.gateway';

@Module({
  imports: [],
  controllers: [AppController],
  providers: [AppService, DatabaseMigrationService, DatabaseService, SocketGateway],
})
export class AppModule {}
