// socket.gateway.ts

import { SubscribeMessage, WebSocketGateway, OnGatewayInit, WebSocketServer } from '@nestjs/websockets';
import { Server, Socket } from 'socket.io';

@WebSocketGateway()
export class SocketGateway implements OnGatewayInit {
  @WebSocketServer() server: Server;

  afterInit(server: Server) {
    console.log('Socket server initialized');
  }

//   @SubscribeMessage('joinRoom')
//   handleJoinRoom(client: Socket, room: string): void {
//     client.join(room);
//     this.server.to(room).emit('userJoined', `A new user has joined the room${room}`);
//   }

    @SubscribeMessage('joinRoom')
    handleJoinRoom(client: Socket, room: string): void {
        console.log(`Пользователь присоединился к комнате ${room}`);
        client.join(room);
        this.server.to(room).emit('userJoined', `A new user has joined the room${room}`);
    }

  @SubscribeMessage('leaveRoom')
  handleLeaveRoom(client: Socket, room: string): void {
    client.leave(room);
    this.server.to(room).emit('userLeft', 'A user has left the room');
  }

  @SubscribeMessage('message')
  handleMessage(client: Socket, payload: any): void {
    console.log('event message: ', payload);
    this.server.emit('message', payload + ' SERVER');
  }
}
