import { Inject, Injectable } from '@nestjs/common';
import { DatabaseService } from 'db/db';

@Injectable()
export class AppService {

  constructor(private readonly databaseService: DatabaseService) {}
    
    async createTask(task: any) {
      const client = await this.databaseService.getClient();
      try {
        const result = await client.query('INSERT INTO tasks (title, description) VALUES ($1, $2)', ['title', 'taskDescription']);
        return result.rows[0];
      } finally {
        this.databaseService.closeClient(client);
      }
    }
  getHello(): string {
    return 'Hello World!';
  }
}
