import { Injectable } from '@nestjs/common';
import { Pool } from 'pg';

@Injectable()
export class TaskService {
 pool: Pool;

 constructor() {
 this.pool = new Pool({
 connectionString: 'postgresql://postgres:1234@localhost:5432/native_postgres',
 ssl: { rejectUnauthorized: false }
 });
 }

 async getClient(): Promise<any> {
 return this.pool.connect();
 }

 closeClient(client: any): void {
 client.release();
 }

 async getAllTasks(): Promise<any> {
 const client = await this.getClient();
 try {
 const result = await client.query('SELECT * FROM tasks');
 return result.rows;
 } finally {
 this.closeClient(client);
 }
 }

 async getTaskById(id: string): Promise<any> {
 const client = await this.getClient();
 try {
 const result = await client.query('SELECT * FROM tasks WHERE id = $1', [id]);
 return result.rows[0];
 } finally {
 this.closeClient(client);
 }
 }
}


import { Injectable } from '@nestjs/common';
import { Pool } from 'pg';

@Injectable()
export class UserService {
 pool: Pool;

 constructor() {
 this.pool = new Pool({
 connectionString: 'postgresql://postgres:1234@localhost:5432/native_postgres',
 ssl: { rejectUnauthorized: false }
 });
 }

 async getClient(): Promise<any> {
 return this.pool.connect();
 }

 closeClient(client: any): void {
 client.release();
 }

 // Методы для работы с моделью User
}
-------------------------------------------
import
 { NestFactory } 
from
 
'@nestjs/core'
;
import
 { AppModule } 
from
 
'./app.module'
;
import
 { DatabaseMigrationService } 
from
 
'./database/database-migration.service'
;
async
 
function
 
bootstrap
(
) 
{
  
const
 app = 
await
 NestFactory.create(AppModule);
  
// Get the DatabaseMigrationService instance

  
const
 databaseMigrationService = app.get(DatabaseMigrationService);
  
// Run the database migration

  
await
 databaseMigrationService.migrateDatabase();
  
await
 app.listen(
3000
);
}
bootstrap();

-----------------------

import
 { Injectable } 
from
 
'@nestjs/common'
;
import
 { Pool } 
from
 
'pg'
;
@Injectable
()
export
 
class
 
DatabaseMigrationService
 
{
  
private
 pool: Pool;
  
constructor
(
private
 databaseService: DatabaseService
)
 {
    
this
.pool = 
this
.databaseService.pool;
  }
  
async
 migrateDatabase(): 
Promise
<
void
> {
    
// Create the database if it doesn't exist

    
await
 
this
.createDatabaseIfNotExists();
    
// Create the necessary tables

    
await
 
this
.createTables();
    
// Seed the database with initial data (optional)

    
await
 
this
.seedDatabase();
  }
  
private
 
async
 createDatabaseIfNotExists(): 
Promise
<
void
> {
    
// Check if the database exists, and create it if it doesn't

    
// You can use a SQL query like "CREATE DATABASE IF NOT EXISTS my_database;"

  }
  
private
 
async
 createTables(): 
Promise
<
void
> {
    
// Create the necessary tables

    
// You can use SQL queries like "CREATE TABLE IF NOT EXISTS users (id INT PRIMARY KEY, name VARCHAR(255));"

  }
  
private
 
async
 seedDatabase(): 
Promise
<
void
> {
    
// Seed the database with initial data

    
// You can use a database seeding library like typeorm-seeding

  }
}