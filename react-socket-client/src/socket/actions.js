const ACTIONS = {
    CONNECT: 'connect',
    CONNECTION: 'connection',
    DISCONNECTING: 'disconnecting',
    JOIN: 'join',
    JOINED: 'joined',
    LEAVE: 'leave',
    SHARE_ROOMS: 'share-rooms',
    ADD_PEER: 'add-peer', // новое соединение между клиентами
    REMOVE_PEER: 'remove-peer',
    RELAY_SDP: 'relay-sdp', // передача медиа данных
    RELAY_ICE: 'relay-ice',
    ICE_CANDIDATE: 'ice-candidate',
    SESSION_DESCRIPTION: 'session-description',

    TEST_ARTICLES: "test-articles",
};

module.exports = ACTIONS;
