import React, { useEffect } from 'react'

export const Main = ({ socket }) => {

  useEffect(() => {
    socket.on('message', (data) => console.log(data));
  }, [socket]);

  const handleSendMessageFromServer = () => {
    socket.emit('message', 'client send message');
  };
  
  return (
    <div>
        <h3>Main</h3>
        <button onClick={handleSendMessageFromServer}>Send Message from server</button>
    </div>
  )
}
