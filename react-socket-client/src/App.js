import { useEffect } from 'react';
import socket from './socket';

import { Main } from './components/Main/Main.jsx';
import { Room } from './components/Room/Room.jsx';

import './App.css';

function App() {
  useEffect(() => {
    console.log('socket', socket);
  }, );
  return (
    <div className="App">
     <h3>SOCKET</h3>
     <Main socket={socket} />
     <Room socket={socket} />
    </div>
  );
}

export default App;
