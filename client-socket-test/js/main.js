import io from '../../node_modules/socket.io-client';

const mainRender = (io) => {
    const options = {
        "force new connection": true,
        reconnectionAttempts: "Infinity",
        timeout : 10000,
        transports : ["websocket"]
    }
    const socket = io.connect(`http://localhost:3000`, options);
    
    socket.on('connect', () => {
        console.log('Connected to server!');
    });
    
    // Добавляем кнопку, которая будет отправлять сообщение на сервер
    const sendButton = document.createElement('button');
    sendButton.textContent = 'Send message';
    sendButton.addEventListener('click', function() {
        // Отправляем сообщение на сервер
        socket.emit('message', 'Hello from client!');
    });
    
    
    document.body.appendChild(sendButton);
};

mainRender(io);

