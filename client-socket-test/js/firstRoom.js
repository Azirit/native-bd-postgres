const joinFirstRoomButton = document.createElement('button');
joinFirstRoomButton.textContent = 'joinFirstRoomButton';
joinFirstRoomButton.addEventListener('click', function() {
    // Отправляем сообщение на сервер
    socket.emit('joinRoom', JSON.stringify({ room: 'roomFirst', message: 'Hello, roomFirst!'}));
});

document.body.appendChild(joinFirstRoomButton);