const joinSecondRoomButton = document.createElement('button');
        
joinSecondRoomButton.textContent = 'joinSecondRoomButton';
joinSecondRoomButton.addEventListener('click', function() {
    // Отправляем сообщение на сервер
    socket.emit('joinRoom', JSON.stringify({ room: 'roomSecond', message: 'Hello, roomSecond!'}));
});

document.body.appendChild(joinSecondRoomButton);