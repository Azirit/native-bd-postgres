import { Injectable } from '@nestjs/common';
import { Pool } from 'pg';
import { DatabaseService } from './db';

@Injectable()
export class DatabaseMigrationService {

  private pool: Pool;

  constructor(private databaseService: DatabaseService) {
    this.pool = this.databaseService.pool;
  }

  async migrateDatabase(): Promise<void> {
    const client = await this.databaseService.getClient();
    await this.createTables(client);
  }

  private async createTables(client: any): Promise<void> {
    try {
        const res1 = await client.query("CREATE TABLE IF NOT EXISTS users (id INT PRIMARY KEY, name VARCHAR(255));")
        const res2 = await client.query("CREATE TABLE IF NOT EXISTS tasks (id INT PRIMARY KEY, name VARCHAR(255));")
    } catch (error) {
        console.error(error);
    }
  }

}
