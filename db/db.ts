import { Injectable } from '@nestjs/common';
import { Pool } from 'pg';

@Injectable()
export class DatabaseService {
    pool: Pool;

    constructor() {
        this.pool = new Pool({
            connectionString: 'postgresql://postgres:1234@localhost:5432/native_postgres',
        });
    }

    async getClient(): Promise<any> {
        return this.pool.connect();
    }

    closeClient(client: any): void {
        client.release();
    }
}